
const int photocell_port = A2;
const int sound_port = A0;

int photorezistor_value;
int sound_value;
String message;
unsigned long time;
unsigned long diff_time;
String tmp;
String word_output;

const char* morse_alphabeth[]={
    ".-", "-...", "-.-.", "-..", ".", "..-.", "--.",  // A-G
    "...." , "..", ".---", "-.-", ".-..", "--", "-.", // H-N
    "---", ".--.", "--.-", ".-.", "...", "-", "..-",  // O-U
    "...-", ".--", "-..-", "-.--", "--.."             // V-Z
  };

void setup() {
  
  pinMode(photocell_port, INPUT);
  pinMode(sound_port, INPUT);
  Serial.begin(9600);
}


char decodeMorse(String morseCode)
{
   for(int i = 0; i < sizeof(morse_alphabeth); i++)
   {
     if(String(morse_alphabeth[i]) == morseCode)
     {
          //Serial.println(i);
          return char(65+i);
     }
      
   }
}


void loop() {
   time = millis();
   //long  - 1s
   //short - .5s 
   
  photorezistor_value = analogRead(photocell_port);

  if( photorezistor_value > 700 ) {

      while((analogRead(photocell_port)) > 400 ) { 

      }

    diff_time = millis()-time;

    if(diff_time >= 3000 )
    {
      Serial.println(word_output);
      word_output="";
    }
    if(diff_time >=  2000 )
    {
      word_output.concat(decodeMorse(tmp));
      tmp = "";
      
    } else if( diff_time >= 500 ) {

      Serial.println("LONG SIGNAL");
      tmp.concat("-");
     
    } else{
      Serial.println("SHORT SIGNAL");
      tmp.concat(".");
    }
      
  }
  
}
