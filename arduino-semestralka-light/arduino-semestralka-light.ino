#include <LiquidCrystal.h>

//sensor pins
const int photocell_sensor = A2;
const int startButton = 2;
const int endButton = 4;

//lcd pins
LiquidCrystal lcd(12,11,10,7,9,8);

//variables
int sensorVal;
unsigned long time;
unsigned long diff_time;
String tmp;
String word_output;

int lightUp;
int lightDown;
int i = 0;

//morse alphabet
const char* morse_alphabeth[]={
    ".-", "-...", "-.-.", "-..", ".", "..-.", "--.",  // A-G
    "...." , "..", ".---", "-.-", ".-..", "--", "-.", // H-N
    "---", ".--.", "--.-", ".-.", "...", "-", "..-",  // O-U
    "...-", ".--", "-..-", "-.--", "--..", "----"            // V-Z
  };
  
char decodeMorse(String morseCode)
{
   for(int i = 0; i < sizeof(morse_alphabeth); i++)
   {
     if(String(morse_alphabeth[i]) == morseCode)
     {
          return char(65+i);
     }
   }

   return '0';
}

void startWelcomeScreen()
{
  lcd.begin(16,3);
  lcd.print("WELCOME");
  
  while(digitalRead(startButton) == 0) {

       if(digitalRead(startButton) == 1)
        {
          break;
        }
     
     
     /*-- scrolling for longer messages -- */
     
     /*for(int i = 0; i < 16; i++)
      { 
        if(digitalRead(startButton) == 1)
        {
          break;
        }
        lcd.scrollDisplayLeft();

        delay(250);
      }*/
  };
  
  lcd.clear();
  lcd.print("STARTED");
  delay(250);
}

void setup() {
  
  Serial.begin(9600);
  pinMode(photocell_sensor, INPUT);
  pinMode(startButton, INPUT);
  pinMode(endButton, INPUT);
  startWelcomeScreen();  
  lcd.clear();
 
  /*--- CALIBRATION ---*/
  TODO
  /*lcd.clear();
  lcd.print("Press start to calibrate for no-light"); 

  delay(150);
   
  while(digitalRead(startButton) == 0) {
    
     for(int i = 0; i < 16; i++)
      { 
        if(digitalRead(startButton) == 1)
        {
          break;
        }
        lcd.scrollDisplayLeft();

        delay(200);
      }
  };

  lightDown = analogRead(photocell_sensor);
  Serial.println("Calibrated");
  Serial.println(lightDown);
  
  lcd.clear();
  lcd.print("Press start to calibrate for light up"); 

  delay(150);

   
  while(digitalRead(startButton) == 0) {
    
     for(int i = 0; i < 16; i++)
      { 
        if(digitalRead(startButton) == 1)
        {
          break;
        }
        lcd.scrollDisplayLeft();

        delay(200);
      }
  };

  lightUp = analogRead(photocell_sensor);
  Serial.println("Calibrated");
  Serial.println(lightUp);*/
  
}

void loop() {

  while(digitalRead(endButton) == 0)
  {
    if(digitalRead(endButton))
    {

      break;
    }
      time = millis();
      //long  - 1s
      //short - .5s 
   
    sensorVal = analogRead(photocell_sensor);

    if( sensorVal > 970 ) {

        while((analogRead(photocell_sensor)) > 940 ) { 

     }

      diff_time = millis()-time;

      if(diff_time >=  2000 )
      {
        lcd.clear();
      
        if(tmp != "----")
        {
          word_output.concat(decodeMorse(tmp));
        } else {
          word_output.concat(" ");
        }
        tmp = "";
        i = 0;
      
      } else if( diff_time >= 500 ) {

        lcd.setCursor(i,0);
        //lcd.clear();
        lcd.print("-");
        tmp.concat("-");
        i++;
     
      } else{

        //signal_output.concat(".");
        lcd.setCursor(i,0);
        //lcd.clear();
        lcd.print(".");
        tmp.concat(".");
        i++;
      }
      
      Serial.println(i);
      }
  }
  
  lcd.clear();
  lcd.print(word_output);
  delay(500);
  
}
  
